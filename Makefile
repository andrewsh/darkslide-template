all: index.html

index.%: slides.%
	ln -sf $< $@

%.html: %.cfg %.md %.css
	darkslide $< -d $@
